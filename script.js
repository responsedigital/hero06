class Hero6 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }

    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse(this.querySelector('script[type="application/json"]').text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initHero6()
    }

    initHero6 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: hero6")
    }

}

window.customElements.define('fir-hero-6', Hero6, { extends: 'section' })
