<?php

namespace Fir\Pinecones\Hero6\Blocks;

use Log1x\AcfComposer\Block;
use StoutLogic\AcfBuilder\FieldsBuilder;
use Fir\Utils\GlobalFields as GlobalFields;
use function Roots\asset;

class Hero6 extends Block
{
    /**
     * The block name.
     *
     * @var string
     */
    public $name = 'Small Header';

    /**
     * The block view.
     *
     * @var string
     */
    public $view = 'Hero6.view';

    /**
     * The block description.
     *
     * @var string
     */
    public $description = '';

    /**
     * The block category.
     *
     * @var string
     */
    public $category = 'fir';

    /**
     * The block icon.
     *
     * @var string|array
     */
    public $icon = 'editor-ul';

    /**
     * The block keywords.
     *
     * @var array
     */
    public $keywords = ['hero6'];

    /**
     * The block post type allow list.
     *
     * @var array
     */
    public $post_types = [];

    /**
     * The parent block type allow list.
     *
     * @var array
     */
    public $parent = [];

    /**
     * The default block mode.
     *
     * @var string
     */
    public $mode = 'preview';

    /**
     * The default block alignment.
     *
     * @var string
     */
    public $align = '';

    /**
     * The default block text alignment.
     *
     * @var string
     */
    public $align_text = '';

    /**
     * The default block content alignment.
     *
     * @var string
     */
    public $align_content = '';

    /**
     * The supported block features.
     *
     * @var array
     */
    public $supports = [
        'align' => array('wide', 'full', 'other'),
        'align_text' => false,
        'align_content' => false,
        'mode' => false,
        'multiple' => true,
        'jsx' => true,
    ];

    /**
     * The block preview example data.
     *
     * @var array
     */
    public $defaults = [
        'content' => [
            'image' => null,
            'wave_color' => 'white',
            'title' => null,
            'intro' => null,
            'cta' => null
        ]
    ];

    /**
     * Data to be passed to the block before rendering.
     *
     * @return array
     */
    public function with()
    {
        $data = $this->parse(get_fields());
        $newData = [
        ];

        return array_merge($data, $newData);
    }

    private function parse($data)
    {
        $data['image'] = ($data['content']['image']) ?: $this->defaults['content']['image'];
        $data['title'] = ($data['content']['title']) ?: $this->defaults['content']['title'];
        $data['intro'] = ($data['content']['intro']) ?: $this->defaults['content']['intro'];
        $data['cta'] = ($data['content']['cta']) ?: $this->defaults['content']['cta'];
        $data['wave_color'] = ($data['content']['wave_color']) ?: $this->defaults['content']['wave_color'];
        $data['flip'] = $data['options']['flip_horizontal'] ? 'hero-6--flip' : '';
        $data['text_align'] = 'hero-6--' . $data['options']['text_align'];
        $data['theme'] = 'fir--' . $data['options']['theme'];
        return $data;
    }

    /**
     * The block field group.
     *
     * @return array
     */
    public function fields()
    {

        $hero6 = new FieldsBuilder('Basic Hero');

        $hero6
            ->addGroup('content', [
                'label' => 'Hero',
                'layout' => 'block'
            ])
                ->addImage('image')
                ->addText('title')
                ->addTextarea('intro')
                ->addLink('cta')
                ->addSelect('wave_color', [
                    'choices' => [
                        'white' => 'White',
                        'blue' => 'Blue',
                        'teal' => 'Light Blue',
                        'gray' => 'Gray'
                    ]
                ])
            ->endGroup()
            ->addGroup('options', [
                'label' => 'Options',
                'layout' => 'block'
            ])
            ->addFields(GlobalFields::getFields('flipHorizontal'))
            ->addFields(GlobalFields::getFields('textAlign'))
            ->addFields(GlobalFields::getFields('theme'))
            ->addFields(GlobalFields::getFields('hideComponent'))
            ->endGroup();

        return $hero6->build();
    }

    /**
     * Assets to be enqueued when rendering the block.
     *
     * @return void
     */
    public function enqueue()
    {
        wp_enqueue_style('sage/app.css', asset('styles/fir/Pinecones/hero6/style.css')->uri(), false, null);
    }
}
