<!-- Start hero6 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : A basic hero banner with title, text, cta & an image. -->
@endif
<section class="{{ $block->classes }}" is="fir-hero-6" id="{{ $pinecone_id ?? '' }}" data-title="{{ $pinecone_title ?? '' }}" data-observe-resizes>
  <img class="hero-6__image" src="{{ $image['url'] }}" alt="{{ $image['alt'] }}">
  <div class="hero-6 {{ $theme }} {{ $text_align }} {{ $flip }}">
    <div class="hero-6__content">
      @if($title)
      <h1 class="hero-6__title">{{ $title }}</h1>
      @endif
      @if($intro)
      <p class="hero-6__text">{{ $intro }}</p>
      @endif
      @if($cta)
      <a class="hero-6__btn btn" href="{{ $cta['url'] }}">{{ $cta['title'] }}</a>
      @endif
    </div>
  </div>
  <svg class="hero-6__wave hero-6__wave--{{ $wave_color }}" viewBox="0 0 1440 76" xmlns="http://www.w3.org/2000/svg">
    <path d="M747.068 26.9562C443.91 62.0689 122.707 41.5865 0 26.9562V76H714H1440V42.6316C1335.34 22.7762 1050.23 -8.15645 747.068 26.9562Z"></path>
  </svg>
</section>
<!-- End hero6 -->
